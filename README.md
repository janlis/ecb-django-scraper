# ECB Django Scraper

A Django (DRF) application allowing to scrap ECB RSS channels on currencies exchange rates. It automatically downloads a list of available RSS channels and stores exchange rate values (vs. Euro) by scraping the RSS channels once a day.

## Requirements
- Python 3
- pip package installer
- Ubuntu 20.04 _(recommended)_

## Quick installation & launch

For a quick installation use the `install.sh` script:
- `source install.sh`

The script will automatically perform all steps listed below, which includes trying to launch the application using Django built-in testing server (the application should be available on `localhost:8000` by default). 

### Manual installation

1. _Recommended_: Create a virtual environment for the application: `python3 -m venv venv`
2. Activate the newly created virtual environment: `source venv/bin/activate`
3. Go to `cd ecb_django_scraper`
4. Install all project's required packages listed in the _requirements.txt_ file: `pip3 install -r requirements.txt`
5. Run Django's DB migrations (initializing the database): `python3 manage.py migrate`

_At this point the application itself can technically be run, but some features require these additional steps:_

6. Subscribe to all currently available ECB RSS channels on currencies exchange rates: `python3 manage.py update-currencies-list`
7. Retrieve the latest exchange rates from subscribed channels: `python3 manage.py get-exchange-rates`
8. Register a Cron task for performing step 7. automatically once a day: `python3 manage.py crontab add`
9. Launch the application: `python3 manage.py runserver`

## Configuration

- The __frequency of exchange rates scraping__ can be customized in `ecb_django_scraper/ecb_django_scraper/settings.py` in the `CRONJOBS` list. In order to apply any changes the tasks need to be re-registered by using:
    - `python3 manage.py crontab remove` _(should print out previously registered tasks)_
    - `python3 manage.py crontab add` _(should print out the newly registered tasks)_

- The ECB RSS channels list url can be modified in `ecb_django_scraper/ecb/conf.py` file, in variables `ECB_RSS_ROOT` and `ECB_RSS_LIST_PATH`.

- The way the scraper tries and finds the RSS channels list and handles the RSS channels links can be altered by modifying:
    - `ECB_CURRENCY_NAME_PATTERN`,
    - `ECB_CURRENCY_TAG_PATTERN` and
    - `ECB_MIN_EXPECTED_CURRENCIES_COUNT`
variables in the `ecb_django_scraper/ecb/conf.py` file.

## Usage & API documentation

The application provides two API endpoints: one for retrieving the list of currently subscribed currencies and one for retrieving the actual exchange rate values for these currencies. The API documentation is available under `/docs` url.
