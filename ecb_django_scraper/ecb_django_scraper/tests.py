from django.core.management import call_command
from django.test import TestCase


class MissingMigrationsCase(TestCase):
    def test_missing_migrations(self):
        """
        All the project models should be synced with
        existing migrations; the `makemigrations` command
        should result in no migrations being made
        """
        result = call_command(
            "makemigrations",
            check=True,
            dry_run=True,
            verbosity=0
        )
        assert not result
