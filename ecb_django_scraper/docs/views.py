from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


def schema_view():
    """
    Returns a drf-yasg generated schema view
    """
    return get_schema_view(
        openapi.Info(
            title="ECB Django Scraper API",
            default_version='v0.1',
            contact=openapi.Contact(email="janlis@foxfactory.pl"),
        ),
        public=True,
        permission_classes=[permissions.AllowAny],
    )
