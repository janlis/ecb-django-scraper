from django.urls import path
from . import views


urlpatterns = [
    path(
        'currencies/',
        views.CurrenciesListView.as_view(),
        name="currencies_list_view"
    ),
    path(
        'currencies/exchange_rates',
        views.CurrencyExchangeRates.as_view(),
        name='currency_exchange_rates_view',
    ),
]
