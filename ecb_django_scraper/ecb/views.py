from rest_framework import generics
from . import models, serializers
from django_filters import rest_framework as filters


class CurrenciesListView(generics.ListAPIView):
    """
    Returns a list of subscribed currencies

    Returns a list of all currencies that are currently subscribed \
    by the application.
    """
    serializer_class = serializers.CurrencySerializer
    queryset = models.Currency.objects.all()


class ExchangeRatesFilter(filters.FilterSet):
    currency_code = filters.CharFilter(
        field_name='currency__code',
        lookup_expr='exact',
        required=True
    )
    from_date = filters.IsoDateTimeFilter(
        field_name='timestamp',
        lookup_expr='gte'
    )
    to_date = filters.IsoDateTimeFilter(
        field_name='timestamp',
        lookup_expr='lte'
    )

    class Meta:
        model = models.ExchangeRate
        fields = ['currency_code', 'from_date', 'to_date']


class CurrencyExchangeRates(generics.ListAPIView):
    """
    Returns saved exchange rates for given currency

    Returns saved exchange rates for given currency based on \
    provided currency code. Currency code can be retrieved from \
    the <code>/currencies/</code> view.
    """
    serializer_class = serializers.ExchangeRateSerializer
    queryset = models.ExchangeRate.objects.all()
    filter_backends = (filters.DjangoFilterBackend, )
    filterset_class = ExchangeRatesFilter
