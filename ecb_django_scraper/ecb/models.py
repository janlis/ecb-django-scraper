from django.db import models


class Currency(models.Model):
    """
    A model representing subscribed ECB currency exchange rate
    RSS channels. Usually imported by scraping the ECB RSS
    channels HTML document using the manage.py update-currencies-list
    command.
    """
    added_at = models.DateTimeField(auto_now_add=True)
    code = models.CharField(
        max_length=4,
        unique=True,
        db_index=True,
        null=False,
    )
    name = models.CharField(max_length=128)
    rss_path = models.CharField(max_length=128, unique=True)


class ExchangeRate(models.Model):
    """
    A model representing single exchange rate data sample
    scraped from ECB RSS channels of subscribed currencies.
    """
    currency = models.ForeignKey(
        Currency,
        on_delete=models.CASCADE,
        related_name="exchange_rates",
        db_index=True,
    )
    timestamp = models.DateTimeField(db_index=True)
    saved_at = models.DateTimeField(auto_now_add=True)
    value = models.DecimalField(decimal_places=4, max_digits=12)

    class Meta:
        unique_together = ['currency', 'timestamp']
        ordering = ['timestamp']
