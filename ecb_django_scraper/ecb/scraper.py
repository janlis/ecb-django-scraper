import logging
import requests
from bs4 import BeautifulSoup as bs

from django.db.utils import IntegrityError
from django.utils import timezone

from . import models, conf


logger = logging.getLogger(__name__)


def fetch_url(url: str) -> bs:
    """
    Performs a GET request for a given url
    and parses the response using BS4
    """
    try:
        response = requests.get(url)
        content = bs(response.content, features='lxml')
        return content
    except Exception as e:
        logger.error("Scraping of %s failed, exception:\n%s" % (url, e))


def parse_ecb_response_content(soup: bs) -> list:
    """
    Transforms a BS html document into a dict containing
    all necessary data
    """
    values_list = []
    items = soup.findAll('item')
    for item in items:
        t = item.find('dc:date').text
        values_list.append({
            'datetime': timezone.datetime.fromisoformat(t),
            'base_currency': item.find('cb:basecurrency').text,
            'target_currency': item.find('cb:targetcurrency').text,
            'value': float(item.find('cb:value').text)
        })
    return values_list


def collect_currencies_exchange_rates() -> None:
    """
    A method for collecting current exchange rates for all
    subscribed currencies by scraping the ECB RSS feeds
    """
    qs = models.Currency.objects.all()
    currencies_list = qs.values_list("id", "rss_path")

    if not currencies_list:
        logger.log(50, "The app is not currently subscribed to any " +
                       "RSS channels. Use the `manage.py " +
                       "update-currencies-list` command.")
        return None
    logger.log(30, "Scraping currencies RSS feeds...")

    # TODO: This part could use some error handling and
    #       should definitely use asynchronous requests instead.
    for currency in currencies_list:
        rss_url = conf.settings.ECB_RSS_ROOT + currency[1]
        response = fetch_url(rss_url)
        item = response.find_all("item", limit=1)[0]
        value = item.find("cb:value").text

        try:
            models.ExchangeRate.objects.create(
                currency_id=currency[0],
                timestamp=item.find("dc:date").text,
                value=value
            )
        except IntegrityError:
            continue
        logger.log(30, "Currency %d exchange rate: %s" % (
            currency[0], value
        ))
