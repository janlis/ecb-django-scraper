
import re
import logging
from bs4 import BeautifulSoup as bs
from bs4.element import Tag

from django.core.management.base import BaseCommand

from ecb.conf import settings
from ecb.scraper import fetch_url
from ecb import models


logger = logging.getLogger(__name__)


class CurrenciesListNotFoundException(Exception):
    pass


class Command(BaseCommand):
    help = """ Scans the ECB RSS links webpage checking
               for currencies list. It only adds new currencies if
               any are found; it will not remove existing currencies
               or collected data. It will ask for confirmation
               before adding found currencies; run with --y parameter
               to skip the confirmation."""

    def add_arguments(self, parser):
        parser.add_argument(
            '--y',
            action='store_true',
            help='Skip confirmation when adding new currencies'
        )

    def find_currencies_list(self, document: bs) -> Tag:
        """
        Search for items lists to find currencies list
        (a list containing multiple a elements with 'rss' in href attribute)
        and a typical text content
        """
        document_lists = document.find_all(("ul", "ol", ))

        def is_list_of_currencies(tag):
            children = tag.findChildren(recursive=False)
            if len(children) < settings.ECB_MIN_EXPECTED_CURRENCIES_COUNT:
                return False
            for elem in children:
                if elem.name != 'li':
                    return False
                elem_children = elem.findChildren(recursive=False)
                if len(elem_children) > 1:
                    return False
                link_elem = elem_children[0]
                if link_elem.name != 'a':
                    return False
                t = link_elem.text
                if not settings.ECB_CURRENCY_NAME_PATTERN.match(t):
                    return False
            return True
        for elem in document_lists:
            if is_list_of_currencies(elem):
                return elem
        raise CurrenciesListNotFoundException

    def handle(self, *args, **options):
        # Retrieve the ECB rss list document
        rss_list_url = settings.ECB_RSS_ROOT + settings.ECB_RSS_LIST_PATH
        html_doc = fetch_url(rss_list_url)

        try:
            currencies_list_elem = self.find_currencies_list(html_doc)
        except CurrenciesListNotFoundException:
            logger.error("""ECB document has been loaded, but the script
                            wasn't able to detect currencies list in it""")
            return

        currencies_list = [[c[0], c[1].strip(), c[2]] for c in re.findall(
            settings.ECB_CURRENCY_TAG_PATTERN,
            str(currencies_list_elem)
        )]
        new_currencies = []

        # Check for currencies that have not been added previously
        for currency in currencies_list:
            if not models.Currency.objects.filter(code=currency[2]).exists():
                new_currencies.append(currency)

        if not new_currencies:
            logger.log(30, ("All currencies (%d) have already been " +
                            "imported. No new currencies found.")
                       % len(currencies_list))
            return

        # Show confirmation before adding new currencies
        # (only if the step is not skipped with -y option passed)
        if not options['y']:
            preview = ", ".join([
                f"{c[1]} ({c[2]}, {c[0]})" for c in new_currencies
            ])
            message = "%d new currencies found:\n\n%s\n\nType 'yes' " + \
                      "if you want to add these currencies to " + \
                      "the subscribtion list: "
            confirmation = input(message % (len(new_currencies), preview))
            if confirmation[0] not in ['y', 'Y']:
                logger.log(30, "\nOperation canceled.")
                return False

        if options['verbosity'] > 1:
            logger.log(30, "Importing currencies from ECB RSS channels list:")

        for currency in new_currencies:
            models.Currency.objects.create(
                code=currency[2],
                name=currency[1],
                rss_path=currency[0]
            )
            if options['verbosity'] > 1:
                logger.log(30, "- %s (%s, path: %s)" % (
                    currency[1], currency[2], currency[0]
                ))

        logger.log(30, "All currencies have been succesfully added.")
