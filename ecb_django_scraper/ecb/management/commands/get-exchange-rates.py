from django.core.management.base import BaseCommand
from ecb.scraper import collect_currencies_exchange_rates


class Command(BaseCommand):
    help = """Calls the ecb.scraper.collect_currencies_exchange_rates
              method which results in the latest exchange rates
              being scraped from ECB RSS feeds that are currently
              subscribed."""

    def add_arguments(self, parser):
        parser.add_argument(
            '--y',
            action='store_true',
            help='Skip confirmation when adding new currencies'
        )

    def handle(self, *args, **options):
        return collect_currencies_exchange_rates()
