import re
from django.conf import settings # noqa
from appconf import AppConf


class ECBConf(AppConf):
    ECB_RSS_ROOT = 'https://www.ecb.europa.eu/'
    ECB_RSS_LIST_PATH = '/rss/'

    ECB_CURRENCY_NAME_PATTERN = re.compile(r"([A-z\s]+)\s?\(([A-Z]{3})\)")
    ECB_CURRENCY_TAG_PATTERN = re.compile(r"<a href=\"([A-z-./]{8,})\">"
                                          r"([A-z\s]{5,})\s\(([A-Z]{3})\)</a>")
    ECB_MIN_EXPECTED_CURRENCIES_COUNT = 10
