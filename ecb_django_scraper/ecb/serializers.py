from rest_framework import serializers
from . import models


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Currency
        fields = ('id', 'code', 'name', )


class ExchangeRateSerializer(serializers.ModelSerializer):
    value = serializers.FloatField()

    class Meta:
        model = models.ExchangeRate
        fields = ('timestamp', 'value')
