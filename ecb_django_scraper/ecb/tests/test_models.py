from django.test import TestCase
from ecb import models

from django.db.utils import IntegrityError


class TestCurrencyModel(TestCase):

    def test_unique_currency_code(self):
        """
        Currency code field must be unique
        Creating another currency instance with existing code should
        result in raising IntegrityError exception
        """
        self.assertEqual(models.Currency.objects.count(), 0)
        with self.assertRaises(IntegrityError):
            models.Currency.objects.create(code="USD")
            models.Currency.objects.create(code="USD")
