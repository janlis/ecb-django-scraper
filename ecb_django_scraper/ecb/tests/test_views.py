from django.conf import settings
from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework.reverse import reverse_lazy
from rest_framework import status

from ecb import models


client = APIClient()


class CurrenciesListViewCase(TestCase):
    fixtures = [f"{settings.BASE_DIR}/ecb/tests/fixtures/test_data.json", ]

    def setUp(self):
        """ Ensure the fixtures are loaded properly """
        self.assertEqual(models.Currency.objects.count(), 32)
        self.assertEqual(models.ExchangeRate.objects.count(), 6)

    def test_currencies_list_request(self):
        """
        Unauthorized user should be able to retrieve currencies list
        """
        client.force_authenticate(user=None)
        url = reverse_lazy("currencies_list_view")
        response = client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertListEqual(
            response.json(),
            list(models.Currency.objects.values('id', 'name', 'code'))
        )


class ExchangeRatesListViewCase(TestCase):
    fixtures = [f"{settings.BASE_DIR}/ecb/tests/fixtures/test_data.json", ]

    def setUp(self):
        return CurrenciesListViewCase.setUp(self)

    def test_required_currency_code_parameter(self):
        """
        Requesting exchange rates list without specifying currency code
        should result in 400 bad request response
        """
        url = reverse_lazy("currency_exchange_rates_view")
        client.get(url)
        response = client.get(url)
        self.assertEqual(
            response.status_code,
            status.HTTP_400_BAD_REQUEST
        )

    def test_exchange_rate_currency_filter(self):
        """
        Requesting exchange rates for a currency should result in
        only returning this currency data
        """
        url = reverse_lazy("currency_exchange_rates_view")

        expected_rates = {
            'USD': [1.0001, 1.0002, 1.0003],
            'JPY': [2.0001, 2.0002],
            'BGN': [3.0001]
        }

        for currency_code in expected_rates:
            r = client.get(url, {'currency_code': currency_code})
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertListEqual(
                [i['value'] for i in r.json()],
                expected_rates[currency_code]
            )

    def test_exchange_rate_datetime_filter(self):
        """
        Requesting exchange rates for a currency while supplying
        date_from and/or date_to values should return a filtered list
        """
        url = reverse_lazy("currency_exchange_rates_view")
        filter_cases = [
            {
                'from_date': '',
                'to_date': '',
                'rates': [1.0001, 1.0002, 1.0003]
            },
            {
                'from_date': '2021-01-02T00:00:00Z',
                'to_date': '',
                'rates': [1.0002, 1.0003]
            },
            {
                'from_date': '',
                'to_date': '2021-01-03T00:00:00Z',
                'rates': [1.0001, 1.0002]
            },
            {
                'from_date': '2021-01-02T00:00:00Z',
                'to_date': '2021-01-03T00:00:00Z',
                'rates': [1.0002]
            }
        ]
        for case in filter_cases:
            data = {
                'currency_code': 'USD',
                'from_date': case['from_date'],
                'to_date': case['to_date']
            }
            r = client.get(url, data)
            self.assertEqual(r.status_code, status.HTTP_200_OK)
            self.assertListEqual(
                [i['value'] for i in r.json()],
                case['rates']
            )
