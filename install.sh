GREEN='\033[1;32m'
NC='\033[0m' # No Color

deactivate || true
printf "\033c"
rm -rf venv || true
printf "${GREEN}Initializing virtual environment...\n${NC}"
python3 -m venv venv
printf "${GREEN}Activating virtual environment...\n${NC}"
source venv/bin/activate
cd ecb_django_scraper
printf "${GREEN}Installing app dependencies...\n${NC}"
pip3 install -r requirements.txt
printf "${GREEN}\n\nInitializing DB...\n${NC}"
python3 manage.py migrate
printf "${GREEN}\n\nRetrieving list of available RSS channels...\n${NC}"
python3 manage.py update-currencies-list --verbosity=2 --y
printf "${GREEN}\n\nRetrieving latest exchange rates...\n${NC}"
python3 manage.py get-exchange-rates
printf "${GREEN}\n\nRegistering cron task...\n${NC}"
python3 manage.py crontab add
printf "${GREEN}\n\nDone. Starting the application...\n${NC}"
python3 manage.py runserver